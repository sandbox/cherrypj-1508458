HTML Private Comments - sometimes users want to add comments to content

-- SUMMARY --

HTML Private Comments is a filter module, helpful on sites which use
WYSIWYG editors.

Sometimes, editors want to add comments to their HTML. Maybe it's for
the next developer; maybe it's to hide content that isn't relevant now,
but will be in the future. This module allows you to add private HTML
comments with [!-- your commented text --] and does so by following the
fundamental Drupal way - non-destructively. These comments are not
visible on the front-end.

-- REQUIREMENTS --

A WYSIWYG editor.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further
information.

-- CONFIGURATION --

* Go to your input formats (/admin/config/content/formats)
* Click the configure link next to the desired input format
* Click the checkbox next to 'HTML Private Comments filter'.
* Under the "Filter processing order", ensure HTML Private Comments
  is at the bottom of the list, unless you know there are other
  filters that need to be processed afterwards.

-- CONTACT --

Updated & maintained by:
* Michael Hessling (cherrypj) - http://drupal.org/user/1138944

From the original, HTMLcomment:
* TBarregren - http://drupal.org/project/htmlcomment